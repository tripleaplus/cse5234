

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class IWorkWithJSPServlet
 */
@WebServlet("/helloServletNew")
public class IWorkWithJSPServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IWorkWithJSPServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String counter = (String) request.getSession().getAttribute("counter");
		if(counter == null){
			counter = String.valueOf(1) ;
		}else{
			int count = Integer.parseInt(counter);
			count++;
			counter = String.valueOf(count);
		}
		request.getSession().setAttribute("counter", counter);

		
		String personalizedGreeting = "Hello "+request.getParameter("name")+"!\n";
		personalizedGreeting += "<br />"+"You visited "+counter+" time(s)!!";
		request.setAttribute("personizedGreeting", personalizedGreeting);
		RequestDispatcher view = getServletContext().getRequestDispatcher("/WEB-INF/jsp/NewGreetingsJSP.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
